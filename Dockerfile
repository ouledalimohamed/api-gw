FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=target/*.jar
COPY target/*.jar app-api.jar
ENTRYPOINT ["java","-jar","/app-api.jar"]